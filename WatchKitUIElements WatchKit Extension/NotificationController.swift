//
//  NotificationController.swift
//  WatchKitUIElements WatchKit Extension
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation
import UserNotifications


class NotificationController : WKUserNotificationInterfaceController {
    
    override func willActivate() {
        // This method is called when the controller is about to be visible to the wearer.
        print("\(self) will activate")
        
        updateUserActivity("com.inheritx.WatchKitUIElements.notification", userInfo: ["Reason" : "Notification"], webpageURL: nil)
    }
    
    override func handleAction(withIdentifier identifier: String?, for notification: UNNotification) {
        print("notification received with identifier: \(identifier), notifiction: \(notification)")
    }
}

//
//  ButtonDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation


class ButtonDetailController: WKInterfaceController {
    @IBOutlet var defaultButton :WKInterfaceButton!
    @IBOutlet var hiddenButton :WKInterfaceButton!
    @IBOutlet var placeholderButton :WKInterfaceButton!
    @IBOutlet var alphaButton :WKInterfaceButton!
    var hidden :Bool
    var placeholderAlpha :CGFloat
    
    override init() {
        hidden = false
        placeholderAlpha = 1.0
    }
    
    @IBAction func hideAndShow() {
        placeholderButton.setHidden(!hidden)
        hidden = !hidden
    }
    
    @IBAction func changeAlpha() {
        placeholderButton.setAlpha(placeholderAlpha == 1.0 ? 0.0 : 1.0)
        placeholderAlpha = (placeholderAlpha == 1.0 ? 0.0 : 1.0)
    }
    
}

//
//  PageTwoController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation


class PageTwoController: WKInterfaceController {
    @IBOutlet var pageLabel :WKInterfaceLabel!

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        pageLabel.setText(String(format:"%@ Page", context as! String))
    }

}

//
//  LabelDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation

class LabelDetailController: WKInterfaceController {
    @IBOutlet var coloredLabel: WKInterfaceLabel!
    @IBOutlet var ultralightLabel: WKInterfaceLabel!
    @IBOutlet var timer: WKInterfaceTimer!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        coloredLabel.setTextColor(UIColor.purple)
        
        let font = UIFont.systemFont(ofSize: 16.0, weight: UIFontWeightUltraLight)
        let attrsDictionary = [NSFontAttributeName : font]
        let attrString = NSMutableAttributedString(string: "Ultra Light Label", attributes: attrsDictionary)
        ultralightLabel.setAttributedText(attrString)
        
        var components = DateComponents()
        components.day = 10
        components.month = 12
        components.year = 2016
        timer.setDate(Calendar.current.date(from: components)!)
        timer.start()
    }
}

//
//  ImageDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit

class ImageDetailController: WKInterfaceController {
    @IBOutlet var staticImage :WKInterfaceImage!
    @IBOutlet var animatedImage :WKInterfaceImage!
    @IBOutlet var playButton :WKInterfaceButton!
    @IBOutlet var stopButton :WKInterfaceButton!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
//        let imageData = UIImagePNGRepresentation(UIImage(named: "Walkway")!)
//        staticImage.setImageData(imageData)
    }
    
    @IBAction func playAnimation() {
        // Uses images in WatchKit app bundle.
        animatedImage.setImageNamed("Bus")
        animatedImage.startAnimating()
    
        // Animate with a specific range, duration, and repeat count.
//        animatedImage.startAnimatingWithImages(in: NSMakeRange(0, 4), duration: 2.0, repeatCount: 3)
    }
    
    @IBAction func stopAnimation() {
        animatedImage.stopAnimating()
    }
}

//
//  DeviceDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit

class DeviceDetailController: WKInterfaceController {
    @IBOutlet var boundsLabel :WKInterfaceLabel!
    @IBOutlet var scaleLabel :WKInterfaceLabel!
    @IBOutlet var preferredContentSizeLabel :WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        let bounds = WKInterfaceDevice.current().screenBounds
        let scale = WKInterfaceDevice.current().screenScale
        
        boundsLabel.setText(NSStringFromCGRect(bounds))
        scaleLabel.setText(NSString(format:"%f",scale) as String)
        preferredContentSizeLabel.setText(WKInterfaceDevice.current().preferredContentSizeCategory)
    }
}

//
//  CrownDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation

class CrownDetailController: WKInterfaceController, WKCrownDelegate {
    @IBOutlet var velocityLabel: WKInterfaceLabel!
    @IBOutlet var stateLabel: WKInterfaceLabel!
    @IBOutlet var pickerView: WKInterfacePicker!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        let itemList: [(String, String)] = [
            ("Item 1", "Red"),
            ("Item 2", "Green"),
            ("Item 3", "Blue")
        ]
        let pickerItems: [WKPickerItem] = itemList.map {
            let pickerItem = WKPickerItem()
            pickerItem.caption = $0.0
            pickerItem.title = $0.1
            return pickerItem
        }
        pickerView.setItems(pickerItems)
        
        crownSequencer.delegate = self
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        crownSequencer.focus()
    }
    
    @IBAction func focusCrown(sender: AnyObject) {
        crownSequencer.focus()
    }
    
    func updateCrownLabels() {
        velocityLabel.setText(String(format: "RPS: %2.2lf", crownSequencer.rotationsPerSecond))
        stateLabel.setText(crownSequencer.isIdle ? "Idle: true" : "Idle: false")
    }
    
    @available(watchOSApplicationExtension 3.0, *)
    func crownDidBecomeIdle(_ crownSequencer: WKCrownSequencer?) {
        updateCrownLabels()
    }
    
    @available(watchOSApplicationExtension 3.0, *)
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        updateCrownLabels()
    }
    
}

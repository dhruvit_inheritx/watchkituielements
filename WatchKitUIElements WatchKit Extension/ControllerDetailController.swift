//
//  ControllerDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit

class ControllerDetailController: WKInterfaceController {
    
    @IBAction func presentPages() {
        let controllerNames = ["PageController","PageTwoController"]
        let contexts = ["First", "Second"]
        
        presentController(withNames: controllerNames, contexts: contexts)
    }
    
    @IBAction func menuItemTapped() {
        print("A menu item was tapped.")
    }
    
}

//
//  SliderDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit

class SliderDetailController: WKInterfaceController {
    @IBOutlet var coloredSlider :WKInterfaceSlider!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        coloredSlider.setColor(UIColor.red)
    }
}

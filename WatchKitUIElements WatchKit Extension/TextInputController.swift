//
//  TextInputController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import WatchConnectivity

class TextInputController: WKInterfaceController {
    
    @IBAction func replyWithTextInputController() {

        let resultHandler = {(results: [Any]?) in
            print("Text Input Results: \(results)")
            if results?.first != nil {
                // Sends a non-nil result to the parent iOS application.
                WCSession.default().sendMessage(["TextInput" : (results?.first)!], replyHandler: { (replyMessage) in
                        print("Reply Info: \(replyMessage)")
                    }, errorHandler: { (error) in
                        print("Error: \(error.localizedDescription)")
                })
            }
        }
        
        // Using the WKTextInputMode enum, you can specify which aspects of the Text Input Controller are shown when presented.
        presentTextInputController(withSuggestions: ["Yes", "No", "Maybe"], allowedInputMode: .allowEmoji, completion: resultHandler)
    }
    
}

//
//  SwitchDetailController.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit

class SwitchDetailController: WKInterfaceController {
    @IBOutlet var offSwitch :WKInterfaceSwitch!
    @IBOutlet var coloredSwitch :WKInterfaceSwitch!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        offSwitch.setOn(false)
        
        coloredSwitch.setColor(UIColor.blue)
        coloredSwitch.setTitle("Blue Switch")
    }
}

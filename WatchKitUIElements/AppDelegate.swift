//
//  AppDelegate.swift
//  WatchKitUIElements
//
//  Created by Dhruvit on 29/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WCSessionDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if #available(iOS 9.0, *) {
            if WCSession.isSupported(){
                WCSession.default().delegate = self
                WCSession.default().activate()
            }
        } else {
            // Fallback on earlier versions
        }
        
        return true
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        print("Handoff dictionary: \(userActivity.userInfo)")
        return true
    }
    
    @available(iOS 9.0, *)
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        /*
         Because this method is likely to be called when the app is in the
         background, begin a background task. Starting a background task ensures
         that your app is not suspended before it has a chance to send its reply.
         */
        
        let application = UIApplication.shared
        
        var identifier = UIBackgroundTaskInvalid;
        // The "endBlock" ensures that the background task is ended and the identifier is reset.
        let endBlock = {
            if identifier != UIBackgroundTaskInvalid {
                application.endBackgroundTask(identifier)
            }
            identifier = UIBackgroundTaskInvalid
        };
        
        identifier = application.beginBackgroundTask(expirationHandler: endBlock)
        
        // Re-assign the "reply" block to include a call to "endBlock" after "reply" is called.
        let replyHandler = {(replyInfo: [String : Any]) in
            replyHandler(replyInfo)
            
            endBlock();
        }
        
        // Receives text input result from the WatchKit app extension.
        print("Message: \(message)")
        
        // Sends a confirmation message to the WatchKit app extension that the text input result was received.
        replyHandler(["Confirmation" : "Text was received."])
    }
    
    @available(iOS 9.3, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    @available(iOS 9.0, *)
    public func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    
    @available(iOS 9.0, *)
    public func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

